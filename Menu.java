import java.util.Scanner;

public class Menu {
    public static void menu_utama() {
//        Scanner pilih = new Scanner(System.in);
        System.out.println("--------------------------------------------------");
        System.out.println("|               Menu Utama                       |");
        System.out.println("--------------------------------------------------");
        System.out.println("1. Menghitung Luas");
        System.out.println("2. Menghitung Volume");
        System.out.println("0. Matikan Program");
        System.out.println("--------------------------------------------------");
        System.out.print("Masukkan angka pilihan menu :");
//        int pilihan = pilih.nextInt();
//        return pilihan;
    }
    public static void sub_menu_luas() {
        System.out.println("--------------------------------------------------");
        System.out.println("|        Pilih bidang datar yang dihtiung        |");
        System.out.println("--------------------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Persegi Panjang");
        System.out.println("3. Lingkaran");
        System.out.println("4. Segitiga");
        System.out.println("0. Kembali ke Menu Utama");
        System.out.println("--------------------------------------------------");
        System.out.print("Masukkan angka pilihan menu :");
    }
    public static void sub_menu_volume(){
        System.out.println("--------------------------------------------------");
        System.out.println("|        Pilih bidang ruang yang dihtiung        |");
        System.out.println("--------------------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke Menu Utama");
        System.out.println("--------------------------------------------------");
    }

}
